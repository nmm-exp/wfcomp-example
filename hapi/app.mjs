import Hapi from "hapi"
import Vision from "vision"
import inert from "inert"
import ejs from "ejs"
import path from "path"
import db from "./db"

!(async function main() {
	const server = new Hapi.Server({ port: 9999 })

	await server.register(inert)
	await server.register(Vision)
	server.views({
		engines: { ejs },
		relativeTo: path.dirname(new URL(import.meta.url).pathname),
		path: "views",
	})

	server.route([
		{
			method: "GET",
			path: "/",
			handler: async (req, h) => {
				const rows = await db.get()
				return h.view("top", { list: rows })
			},
		},
		{
			method: "GET",
			path: "/resources/{param*}",
			handler: {
				directory: {
					path: "resources",
				},
			},
		},
		{
			method: "GET",
			path: "/search",
			handler: async (req, h) => {
				const search_query = req.query.q || ""
				const rows = await db.search(search_query)
				return h.view("search", { list: rows, query: search_query })
			},
		},
		{
			method: "GET",
			path: "/view/{id}",
			handler: async (req, h) => {
				const row = await db.get(req.params.id)
				if (row) {
					return h.view("view", { item: row })
				} else {
					return h.view("error", { message: `${req.params.id} is not created.` }).code(404)
				}
			},
		},
		{
			method: "GET",
			path: "/edit/{id}",
			handler: async (req, h) => {
				const row = (await db.get(req.params.id)) || { id: req.params.id, no_data: true }
				return h.view("edit", { item: row })
			},
		},
		{
			method: "POST",
			path: "/edit/{id}",
			handler: async (req, h) => {
				const obj = JSON.parse(req.payload)
				await db.set(req.params.id, obj)
				return { redirect: "/view/" + req.params.id }
			},
		},
		{
			method: "POST",
			path: "/delete/{id}",
			handler: async (req, h) => {
				db.delete(req.params.id)
				return { redirect: "/" }
			},
		},
		{
			method: "*",
			path: "/{path*}",
			handler: async (req, h) => {
				return h.view("error", { message: "" }).code(404)
			},
		},
	])

	await server.start()
	console.log("start")
})()
