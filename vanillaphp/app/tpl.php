<?php

class Tpl
{
    public static $tpl;
    private $path;
    private $data;
    private $base = null;
    private $current_section = null;
    private $sections = [];

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function render($data)
    {
        $this->data = $data;
        $current = self::$tpl;
        self::$tpl = $this;
        $___filepath___ = $this->path;
        $___data___ = $data;
        ob_start();
        call_user_func(function () use ($___data___, $___filepath___) {
            extract($___data___);
            require $___filepath___;
        });
        self::$tpl = $current;
        return ob_get_clean();
    }

    public function use($path)
    {
        if ($this->base) {
            throw new Exception('use cannot be nested');
        }
        $tpl = new Tpl($path);
        $this->base = $tpl;
        ob_start();
    }
    
    public function useEnd()
    {
        if ($this->base === null) {
            throw new Exception('No use file.');
        }
        ob_end_clean();
        echo $this->base->render(array_merge($this->data, $this->sections));
        $this->base = null;
        $this->sections = [];
    }

    public function section($name)
    {
        if ($this->current_section) {
            throw new Exception('section cannot be nested');
        }
        $this->current_section = $name;
        ob_start();
    }

    public function sectionEnd()
    {
        $name = $this->current_section;
        if ($name === null) {
            throw new Exception('section not found');
        }
        $this->current_section = null;
        $data = ob_get_clean();
        $this->sections[$name] = $data;
    }

    public function var($name, $value)
    {
        $this->data[$name] = $value;
    }
}

function section($name)
{
    Tpl::$tpl->section($name);
}

function sectionend()
{
    Tpl::$tpl->sectionEnd();
}

function usetpl($name)
{
    Tpl::$tpl->use($name);
}

function usetplend()
{
    Tpl::$tpl->useEnd();
}

function setvar($name, $value)
{
    Tpl::$tpl->var($name, $value);
}

function h($text)
{
    return htmlspecialchars($text);
}
