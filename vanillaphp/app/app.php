<?php

define('ROOT', __DIR__);

require_once ROOT . '/db.php';
require_once ROOT . '/tpl.php';

Db::$instance->init();

$actions = require ROOT . '/actions.php';
$path_str = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
$path = array_slice(explode('/', $path_str), 1);
$method = $_SERVER['REQUEST_METHOD'];

// URL と メソッドにマッチする実行内容の取得
$action = null;
$captures = [];
foreach ($actions as $v) {
    if (isset($v['path_condition'])) {
        $i = 0;
        $passed = 0;
        $captures = [];
        foreach ($v['path_condition'] as $c) {
            $p = $path[$i++];
            // 最初と最後が / なら正規表現扱い
            if (preg_match('@^/.*/$@', $c)) {
                if (preg_match($c, $p, $m)) {
                    $captures += array_filter($m, function ($k) {
                        return !is_int($k);
                    }, ARRAY_FILTER_USE_KEY);
                } else {
                    break;
                }
            } else {
                if ($p !== $c) {
                    break;
                }
            }
            $passed++;
        }
        $matched = $i === $passed;
        if (!$matched) {
            continue;
        }
    }
    
    if (isset($v['method_condition'])) {
        $cond = $v['method_condition'];
        $matched = is_array($cond) ? in_array($method, $cond) : $method === $cond;
        if (!$matched) {
            continue;
        }
    }

    $action = $v['action'];
    break;
}

$body = $action([
    'path' => $path,
    'captures' => $captures,
]);

if ($body) {
    echo $body;
}
