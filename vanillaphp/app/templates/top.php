<? usetpl(__DIR__ ."/base.php") ?>

<? setvar("title", "top"); ?>

<? section("head"); ?>
<style>
</style>
<? sectionend(); ?>

<? section("body"); ?>
<h1>List</h1>
<ul>
	<? foreach($list as $item): ?>
		<li><a href="/view/<?= h($item['id']) ?>"><?= h($item['title']) ?></a></li>
	<? endforeach ?>
</ul>
<? sectionend(); ?>

<? usetplend(); ?>
