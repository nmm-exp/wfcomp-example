import { tpl, h } from "../tpl"
import base from "./base"

const head = `
<script src="https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.19/marked.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.min.css">
<style>
#title {margin: 5px 0; width: 100%;}
h1 span {font-size: 12px; color: silver;}
.btns {text-align: right;}
</style>
`

const body = tpl`
<h1>${"title"} <span>(${"jdate"})</span></h1>
<div class="btns"><button id="edit">Edit</button></div>
<div id="body" class="markdown-body"></div>
<template id="md">${"body"}</template>
<script>
	document.querySelector("#body").innerHTML = marked(document.querySelector("#md").content.textContent)
	document.querySelector("#edit").addEventListener("click", eve => location.href = location.href.replace("view", "edit"))
</script>
`

export default function(data) {
	return base({
		title: data.item.title,
		head,
		body: body(data.item),
	})
}
