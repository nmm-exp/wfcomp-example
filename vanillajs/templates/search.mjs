import { tpl, h } from "../tpl"
import base from "./base"

const head = `
<style>
</style>
`

const body = tpl`
<h1>Search result of "${"query"}"</h1>
<div>
	<form>
		<input id="search" name="q" value="${"query"}" />
		<button type="submit">Search</button>
	</form>
</div>
<hr>
<ul>
	${function list(values = []) {
		return values
			.map(e =>
				tpl`
					<li><a href="/view/${"id"}">${"title"}</a></li>
				`(e)
			)
			.join("")
	}}
</ul>
`

export default function(data) {
	return base({
		title: "Search - " + data.query,
		head,
		body: body(data),
	})
}
