import { tpl, h } from "../tpl"

const fn = tpl`
<!doctype html>
<title>${"title"}</title>
<script src="/resources/common.js" type="module"></script>
<link rel="stylesheet" href="/resources/common.css" />
${"!head"}
<div id="container">
	${"!body"}
</div>
`

export default function(data) {
	return fn(data)
}
