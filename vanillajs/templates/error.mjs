import { tpl, h } from "../tpl"
import base from "./base"

const head = `
`

const body = tpl`
<h1>404 PageNotFound</h1>
<p>${"message"}</p>
`

export default function(data) {
	return base({
		title: "error",
		head,
		body: body(data),
	})
}
