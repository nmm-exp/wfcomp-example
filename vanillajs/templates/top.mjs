import { tpl, h } from "../tpl"
import base from "./base"

const head = `
<style>
</style>
`

const body = tpl`
<h1>List</h1>
<ul>
	${function list(values = []) {
		return values
			.map(e =>
				tpl`
					<li><a href="/view/${"id"}">${"title"}</a></li>
				`(e)
			)
			.join("")
	}}
</ul>
`

export default function(data) {
	return base({
		title: "Top",
		head,
		body: body(data),
	})
}
