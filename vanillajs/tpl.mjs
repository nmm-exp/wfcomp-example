export { tpl, h }

function tpl(parts, ...values) {
	return data => {
		return parts.reduce((a, e, i) => {
			const value = values[i - 1]
			const text = (function() {
				if (typeof value === "function") {
					const fn = value
					return fn.name in data ? fn(data[fn.name]) : fn()
				} else {
					const name = value + ""
					// ! から始まるのはエスケープしない
					if (name.startsWith("!")) {
						const realname = name.substr(1)
						return realname in data ? data[realname] : ""
					} else {
						return name in data ? h(data[name]) : ""
					}
				}
			})()
			return a + text + e
		})
	}
}

function h(str) {
	return (str == null ? "" : String(str))
		.replace(/&/g, "&amp;")
		.replace(/"/g, "&quot;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
}
