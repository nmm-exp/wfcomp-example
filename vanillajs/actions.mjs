import fs from "fs"
import path from "path"
import db from "./db"

export default [
	{
		title: "top",
		path_condition: [""],
		async action() {
			const rows = await db.get()
			const { default: render } = await import("./templates/top.mjs")
			return render({ list: rows })
		},
	},
	{
		title: "resources",
		path_condition: ["resources"],
		action({ path: path_arr, res }) {
			const rpath = path_arr.join("/")
			if (!fs.existsSync(rpath)) {
				res.writeHead(404)
				return "FileNotFound"
			}
			const ext_mime = {
				".html": "text/html",
				".css": "text/css",
				".js": "text/javascript",
			}
			res.writeHead(200, {
				"Content-Type": ext_mime[path.extname(rpath)],
			})
			fs.createReadStream(rpath).pipe(res)
			return true
		},
	},
	{
		title: "search",
		path_condition: ["search"],
		method_condition: "GET",
		async action({ query }) {
			const search_query = query.get("q") || ""
			const rows = await db.search(search_query)
			const { default: render } = await import("./templates/search.mjs")
			return render({ list: rows, query: search_query })
		},
	},
	{
		title: "view",
		path_condition: ["view", /^(?<id>.+)$/],
		async action({ captures }) {
			const row = await db.get(captures.id)
			if (row) {
				const { default: render } = await import("./templates/view.mjs")
				return render({ item: row })
			} else {
				res.writeHead(404, { "Content-Type": "text/html" })
				const { default: render } = await import("./templates/error.mjs")
				return render({ message: `${captures.id} is not created.` })
			}
		},
	},
	{
		title: "edit",
		path_condition: ["edit", /^(?<id>.+)$/],
		method_condition: "GET",
		async action({ captures }) {
			const row = (await db.get(captures.id)) || { id: captures.id, no_data: true }
			const { default: render } = await import("./templates/edit.mjs")
			return render({ item: row })
		},
	},
	{
		title: "edit_update",
		path_condition: ["edit", /^(?<id>.+)$/],
		method_condition: "POST",
		async action({ res, post, captures }) {
			const obj = JSON.parse(await post)
			await db.set(captures.id, obj)
			res.writeHead(200, {
				"Content-Type": "text/json",
			})
			return JSON.stringify({ redirect: "/view/" + captures.id })
		},
	},
	{
		title: "delete",
		path_condition: ["delete", /^(?<id>.+)$/],
		method_condition: "POST",
		async action({ res, captures }) {
			db.delete(captures.id)
			res.writeHead(200, {
				"Content-Type": "text/json",
			})
			return JSON.stringify({ redirect: "/" })
		},
	},
	{
		title: "default",
		async action({ res }) {
			res.writeHead(404, { "Content-Type": "text/html" })
			const { default: render } = await import("./templates/error.mjs")
			return render({})
		},
	},
]
