import express from "express"
import sqlite3 from "sqlite3"
import getRawBody from "raw-body"
import db from "./db"

const app = express()
db.init()

app.set("view engine", "ejs")
app.use("/resources", express.static("resources"))

app.get("/", async (req, res) => {
	const rows = await db.get()
	res.render("top", { list: rows })
})

app.get("/search", async (req, res) => {
	const search_query = req.query.q || ""
	const rows = await db.search(search_query)
	res.render("search", { list: rows, query: search_query })
})

app.get("/view/:id", async (req, res) => {
	const row = await db.get(req.params.id)
	if (row) {
		return res.render("view", { item: row })
	} else {
		return res.status(404).render("error", { message: `${req.params.id} is not created.` })
	}
})

app.get("/edit/:id", async (req, res) => {
	const row = (await db.get(req.params.id)) || { id: req.params.id, no_data: true }
	res.render("edit", { item: row })
})

app.post("/edit/:id", async (req, res) => {
	const buf = await getRawBody(req)
	const obj = JSON.parse(buf.toString())
	await db.set(req.params.id, obj)
	res.json({ redirect: "/view/" + req.params.id })
})

app.post("/delete/:id", async (req, res) => {
	db.delete(req.params.id)
	res.json({ redirect: "/" })
})

app.all("*", async (req, res) => {
	res.status(404).render("error", {message: ""})
})

app.listen(9999, () => console.log("start"))
