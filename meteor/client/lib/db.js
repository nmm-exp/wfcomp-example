const createApplier = method_name => {
	return new Proxy(
		{},
		{
			get(target, name) {
				return (...args) => {
					return new Promise((ok, ng) => {
						Meteor.apply(method_name, [name, ...args], (err, result) => {
							if (err) ng(err)
							else ok(result)
						})
					})
				}
			},
		}
	)
}
export default createApplier("db_operation")
