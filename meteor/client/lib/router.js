import db from "./db"

FlowRouter.route("/", {
	async action() {
		const rows = await db.get()
		BlazeLayout.render("top", { list: rows })
	},
})

FlowRouter.route("/search", {
	async action(params, query_params) {
		const search_query = query_params.q || ""
		const rows = await db.search(search_query)
		BlazeLayout.render("search", { list: rows, query: search_query })
	},
})

FlowRouter.route("/view/:id", {
	async action(params, query_params) {
		const row = await db.get(params.id)
		if (row) {
			row.html = marked(row.body)
			return BlazeLayout.render("view", { item: row, foo: 100 })
		} else {
			return BlazeLayout.render("error", { message: `${params.id} is not created.` })
		}
	},
})

FlowRouter.route("/edit/:id", {
	async action(params, query_params) {
		const row = (await db.get(params.id)) || { id: params.id, no_data: true }
		BlazeLayout.render("edit", { item: row })
	},
})

FlowRouter.notFound = {
	action() {
		BlazeLayout.render("error", { message: "" })
	},
}
