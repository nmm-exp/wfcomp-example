import Koa from "koa"
import KoaRouter from "koa-router"
import views from "koa-views"
import koaBody from "koa-body"
import serve from "koa-static"
import sqlite3 from "sqlite3"
import path from "path"
import db from "./db"

const app = new Koa()
const router = new KoaRouter()
db.init()

const dirname = path.dirname(new URL(import.meta.url).pathname)
app.use(views(path.join(dirname, "/views"), { extension: "ejs" }))
app.use(serve(path.join(dirname, "/public")))

app.use(koaBody())

router
	.get("/", async (ctx, next) => {
		const rows = await db.get()
		await ctx.render("top", { list: rows })
	})
	.get("/search", async (ctx, next) => {
		const search_query = ctx.request.query.q || ""
		const rows = await db.search(search_query)
		await ctx.render("search", { list: rows, query: search_query })
	})
	.get("/view/:id", async (ctx, next) => {
		const row = await db.get(ctx.params.id)
		if (row) {
			return await ctx.render("view", { item: row })
		} else {
			ctx.status = 404
			return await ctx.render("error", { message: `${ctx.params.id} is not created.` })
		}
	})
	.get("/edit/:id", async (ctx, next) => {
		const row = (await db.get(ctx.params.id)) || { id: ctx.params.id, no_data: true }
		await ctx.render("edit", { item: row })
	})
	.post("/edit/:id", async (ctx, next) => {
		const obj = JSON.parse(ctx.request.body)
		await db.set(ctx.params.id, obj)
		ctx.body = { redirect: "/view/" + ctx.params.id }
	})
	.post("/delete/:id", async (ctx, next) => {
		db.delete(ctx.params.id)
		ctx.body = { redirect: "/" }
	})
	.all("*", async (ctx, next) => {
		ctx.status = 404
		ctx.render("error", { message: "" })
	})

app.use(router.routes())
app.listen(9999)
