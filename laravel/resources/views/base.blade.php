<!doctype html>
<title>@yield('title')</title>
<script src="/resources/common.js" type="module"></script>
<link rel="stylesheet" href="/resources/common.css" />
@yield('head')
<div id="container">
	@yield('body')
</div>
