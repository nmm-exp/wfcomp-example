@extends('base')

@section('title', 'top')

@section('head')
<style>
</style>
@endsection

@section('body')
<h1>List</h1>
<ul>
    @foreach ($list as $item)
        <li><a href="/view/{{ $item['id'] }}">{{ $item['title'] }}</a></li>
    @endforeach
</ul>
@endsection
