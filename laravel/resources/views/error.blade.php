@extends('base')

@section('title', 'error')

@section('head', '')

@section('body')
<h1>404 PageNotFound</h1>
<p>{{ $message }}</p>
@endsection
