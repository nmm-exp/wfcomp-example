<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Db;

Route::get('/', function () {
    $rows = Db::$instance->get();
    return view('top', ['list' => $rows]);
});

Route::get('/search', function () {
    $query = Request::query('q', '');
    $rows = Db::$instance->search($query);
    return view('search', ['list' => $rows, 'query' => $query]);
});

Route::get('/view/{id}', function ($id) {
    $row = Db::$instance->get($id);
    if ($row) {
        return view('view', ['item' => $row]);
    } else {
        return response()
            ->view('error', ['message' => "{$id} is not created."], 404);
    }
});

Route::get('/edit/{id}', function ($id) {
    $row = Db::$instance->get($id);
    if (!$row) {
        $row = ['id' => $id, 'no_data' => true];
    }
    return view('edit', ['item' => $row]);
});

Route::post('/edit/{id}', function ($id) {
    $post = Request::instance()->getContent();
    $obj = json_decode($post, true);
    Db::$instance->set($id, $obj);
    return ['redirect' => "/view/{$id}"];
});

Route::post('/delete/{id}', function ($id) {
    Db::$instance->delete($id);
    return ['redirect' => '/'];
});

Route::any('/{all}', function () {
    return response()
        ->view('error', ['message' => ''], 404);
});
