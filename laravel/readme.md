# How to start

public をドキュメントルートにする

```
php -S 0.0.0.0:9999 -t public
```

# Changes from template

```
* -- edit
+ -- add

---

* .gitattribute
    text=auto いらない

* app/Http/Middleware/VerifyCsrfToken.php
    csrf トークンは使わない

* bootstrap/app.php
    起動時の DB 初期化

+ public/resources/common.css
+ public/resources/common.js
    他のと同じ

+ resources/views/base.blade.php
+ resources/views/edit.blade.php
+ resources/views/error.blade.php
+ resources/views/search.blade.php
+ resources/views/top.blade.php
+ resources/views/view.blade.php
    blade テンプレート

* routes/web.php
    routing
```
